﻿using System;
using System.Linq;
using System.Xml.Linq;

namespace vfd_service
{
    public class xmlAccess
    {
        static XDocument vfdselection = XDocument.Load(@Environment.CurrentDirectory + "\\..\\..\\..\\..\\vfdselection.xml");
        static XDocument motorselection = XDocument.Load(@Environment.CurrentDirectory + "\\..\\..\\..\\..\\motorselection.xml");
        static XDocument serviceVFD = XDocument.Load(@Environment.CurrentDirectory + "\\..\\..\\..\\..\\serviceVFD.xml");
        static XDocument serviceMotor = XDocument.Load(@Environment.CurrentDirectory + "\\..\\..\\..\\..\\serviceMotor.xml");

        public static void addVFDElement(string vfd, string newElement)
        {
            vfdselection?.Root?.Element(vfd)?.Add(new XElement(newElement, ""));
            vfdselection.Save(@Environment.CurrentDirectory + "\\..\\..\\..\\..\\vfdselection.xml");
        }

        public static void addMotorElement(string motor, string newElement)
        {
            motorselection?.Root?.Element(motor)?.Add(new XElement(newElement, ""));
            motorselection.Save(@Environment.CurrentDirectory + "\\..\\..\\..\\..\\motorselection.xml");
        }

        public static void addVFDElementValue(string vfd, string element, string address)
        {
            vfdselection?.Root?.Element(vfd)?.SetElementValue(element, address);
            vfdselection.Save(@Environment.CurrentDirectory + "\\..\\..\\..\\..\\vfdselection.xml");
        }

        public static void addMotorElementValue(string motor, string element, string address)
        {
            motorselection?.Root?.Element(motor)?.SetElementValue(element, address);
            motorselection.Save(@Environment.CurrentDirectory + "\\..\\..\\..\\..\\motorselection.xml");
        }

        public static void addVFD(string vfdName)
        {
            XElement root = new XElement(vfdName);
            vfdselection?.Element("Root")?.Add(root);
            vfdselection.Save(@Environment.CurrentDirectory + "\\..\\..\\..\\..\\vfdselection.xml");
        }

        public static void removeVFD(string vfdName)
        {
            vfdselection.Root.Descendants().Where(e => e.Name == vfdName).Remove();
            vfdselection.Save(@Environment.CurrentDirectory + "\\..\\..\\..\\..\\vfdselection.xml");
        }

        public static void addMotor(string motor)
        {
            XElement root = new XElement(motor);
            motorselection?.Element("Root")?.Add(root);
            motorselection.Save(@Environment.CurrentDirectory + "\\..\\..\\..\\..\\motorselection.xml");
        }

        public static void removeMotor(string motorName)
        {
            motorselection.Root.Descendants().Where(e => e.Name == motorName).Remove();
            motorselection.Save(@Environment.CurrentDirectory + "\\..\\..\\..\\..\\motorselection.xml");
        }

        public static int getVFDElementAddress(string element)
        {
            return Int32.Parse(serviceVFD?.Root?.Descendants()?.First()?.Element(element)?.Value);
        }

        public static int getMotorElementValue(string element)
        {
            return Int32.Parse(serviceMotor?.Root?.Descendants()?.First()?.Element(element)?.Value);
        }

        public static void addVFDSettings(string vfd)
        {
            addVFDElement(vfd, "command");
            addVFDElement(vfd, "maxRPM");
            addVFDElement(vfd, "maxFrequency");
            addVFDElement(vfd, "horsepower");
            addVFDElement(vfd, "maxAmps");
            addVFDElement(vfd, "voltage");
            addVFDElement(vfd, "acceleration");
            addVFDElement(vfd, "deceleration");
            addVFDElement(vfd, "speedSetPoint");
            addVFDElement(vfd, "outputFrequency");
            addVFDElement(vfd, "outputVelocity");
            addVFDElement(vfd, "driveThermalState");
            addVFDElement(vfd, "motorThermalState");
            addVFDElement(vfd, "totalDriveOperatingTime");
            addVFDElement(vfd, "totalMotorOperatingTime");
            addVFDElement(vfd, "motorTorque");
            addVFDElement(vfd, "motorCurrent");
            addVFDElement(vfd, "motorVoltage");
            addVFDElement(vfd, "faultCodes");
            addVFDElement(vfd, "driveState");
        }

        public static void addMotorSettings(string motor)
        {
            addMotorElement(motor, "maxRPM");
            addMotorElement(motor, "maxFrequency");
            addMotorElement(motor, "horsepower");
            addMotorElement(motor, "maxAmps");
            addMotorElement(motor, "voltage");
            addMotorElement(motor, "acceleration");
            addMotorElement(motor, "deceleration");
            addMotorElement(motor, "speedSetPoint");
        }
        //Adding a value to each of the addresses (adding values to the xml tags)
        public static void addVFDAddresses(string vfd, string com, string rpm, string maxFreq, string hp, string maxAmp, string volt, string accel,
                                    string decel, string speedsetpoint, string outputFreq, string outputvelocity,
                                    string drivethermalstate, string motorthermalstate, string totaldriveoperatingtime,
                                    string totalmotoroperatingtime, string motortorque, string motorcurrent, string motorvoltage,
                                    string faultcodes, string driveState)
        {
            addVFDElementValue(vfd, "command", com);
            addVFDElementValue(vfd, "maxRPM", rpm);
            addVFDElementValue(vfd, "maxFrequency", maxFreq);
            addVFDElementValue(vfd, "horsepower", hp);
            addVFDElementValue(vfd, "maxAmps", maxAmp);
            addVFDElementValue(vfd, "voltage", volt);
            addVFDElementValue(vfd, "acceleration", accel);
            addVFDElementValue(vfd, "deceleration", decel);
            addVFDElementValue(vfd, "speedSetPoint", speedsetpoint);
            addVFDElementValue(vfd, "outputFrequency", outputFreq);
            addVFDElementValue(vfd, "outputVelocity", outputvelocity);
            addVFDElementValue(vfd, "driveThermalState", drivethermalstate);
            addVFDElementValue(vfd, "motorThermalState", motorthermalstate);
            addVFDElementValue(vfd, "totalDriveOperatingTime", totaldriveoperatingtime);
            addVFDElementValue(vfd, "totalMotorOperatingTime", totalmotoroperatingtime);
            addVFDElementValue(vfd, "motorTorque", motortorque);
            addVFDElementValue(vfd, "motorCurrent", motorcurrent);
            addVFDElementValue(vfd, "motorVoltage", motorvoltage);
            addVFDElementValue(vfd, "faultCodes", faultcodes);
            addVFDElementValue(vfd, "driveState", driveState);
        }
        //Adding values to the motor elements (adding values to the xml tags)
        public static void addMotorElementValues(string motor, string maxRPM, string maxFrequency, string hp, string maxAmps, string voltage,
                                                string accel, string decel, string speedSetPoint)
        {
            addMotorElementValue(motor, "maxRPM", maxRPM);
            addMotorElementValue(motor, "maxFrequency", maxFrequency);
            addMotorElementValue(motor, "horsepower", hp);
            addMotorElementValue(motor, "maxAmps", maxAmps);
            addMotorElementValue(motor, "voltage", voltage);
            addMotorElementValue(motor, "acceleration", accel);
            addMotorElementValue(motor, "deceleration", decel);
            addMotorElementValue(motor, "speedSetPoint", speedSetPoint);
        }
        //set VFD to use
        public static void setServiceVFD(string name)
        {
            String header = @"<?xml version=""1.0"" encoding=""utf-8""?><Root>";
            String back = @"</Root>";
            String contents = header + vfdselection?.Root?.Element(name)?.ToString() + back;
            System.IO.File.WriteAllText(@Environment.CurrentDirectory + "\\..\\..\\..\\..\\serviceVFD.xml", contents); //output settings to this file for the service to access
        }
        //set motor to use
        public static void setServiceMotor(string name)
        {
            String header = @"<?xml version=""1.0"" encoding=""utf-8""?><Root>";
            String back = @"</Root>";
            String contents = header + motorselection?.Root?.Element(name)?.ToString() + back;
            System.IO.File.WriteAllText(@Environment.CurrentDirectory + "\\..\\..\\..\\..\\serviceMotor.xml", contents); //output settings to this file for the service to access
        }
    }
}
