﻿using System;
using System.Collections.Generic;

namespace vfd_service
{
    public class vfd
    {
        //Declare the modbusClient object
        public EasyModbus.ModbusClient modbusClient;
        //Dictionary to hold the addresses
        IDictionary<string, int> addresses = new Dictionary<string, int>();

        private enum commands { shutdown = 6, switchOn = 7, enableOperation = 15, resetFaults = 128, haltStop = 395 };

        //constructor
        public vfd(EasyModbus.ModbusClient inModbusClient) //initiate modbus client
        {
            modbusClient = inModbusClient;
            modbusClient.Connect();
            //Setting addresses from xmlAdresses.xml file
            addresses.Add("command", xmlAccess.getVFDElementAddress("command"));
            addresses.Add("maxRPM", xmlAccess.getVFDElementAddress("maxRPM"));
            addresses.Add("maxFrequency", xmlAccess.getVFDElementAddress("maxFrequency"));
            addresses.Add("horsepower", xmlAccess.getVFDElementAddress("horsepower"));
            addresses.Add("maxAmps", xmlAccess.getVFDElementAddress("maxAmps"));
            addresses.Add("voltage", xmlAccess.getVFDElementAddress("voltage"));
            addresses.Add("acceleration", xmlAccess.getVFDElementAddress("acceleration"));
            addresses.Add("deceleration", xmlAccess.getVFDElementAddress("deceleration"));
            addresses.Add("speedSetPoint", xmlAccess.getVFDElementAddress("speedSetPoint"));
            addresses.Add("outputFrequency", xmlAccess.getVFDElementAddress("outputFrequency"));
            addresses.Add("outputVelocity", xmlAccess.getVFDElementAddress("outputVelocity"));
            addresses.Add("driveThermalState", xmlAccess.getVFDElementAddress("driveThermalState"));
            addresses.Add("motorThermalState", xmlAccess.getVFDElementAddress("motorThermalState"));
            addresses.Add("totalDriveOperatingTime", xmlAccess.getVFDElementAddress("totalDriveOperatingTime"));
            addresses.Add("totalMotorOperatingTime", xmlAccess.getVFDElementAddress("totalMotorOperatingTime"));
            addresses.Add("motorTorque", xmlAccess.getVFDElementAddress("motorTorque"));
            addresses.Add("motorCurrent", xmlAccess.getVFDElementAddress("motorCurrent"));
            addresses.Add("motorVoltage", xmlAccess.getVFDElementAddress("motorVoltage"));
            addresses.Add("faultCodes", xmlAccess.getVFDElementAddress("faultCodes"));
            addresses.Add("driveState", xmlAccess.getVFDElementAddress("driveState"));
        }

        public void initializeMotorAttributes()
        {
            //setMotorElement(string)
            writeToAddress("maxRPM", 1);
            writeToAddress("maxFrequency", 10); //Rated motor freq. in .1 Hz
            writeToAddress("horsepower", 1); //Rated motor power in 10*hp, so .3 hp appears as 3 hp
            writeToAddress("maxAmps", 10); //Rated motor current in .1 A, putting in 10 * 1
            writeToAddress("voltage", 1);
            writeToAddress("acceleration", 1);
            writeToAddress("deceleration", 1);
            writeToAddress("speedSetPoint", 1);
        }

        public void writeToAddress(string attribute, int multiplier)
        {
            modbusClient.WriteMultipleRegisters(addresses[attribute], new int[1] { multiplier * xmlAccess.getMotorElementValue(attribute) });
        }

        public void writeToCommand(int command)
        {
            modbusClient.WriteMultipleRegisters(addresses["command"], new int[1] { command });
        }

        public int readFromAddress(string attribute)
        {
            return modbusClient.ReadHoldingRegisters(addresses[attribute], 1)[0];
        }

        public void startMotor()
        {
            writeToAddress("speedSetPoint", 1);
            writeToCommand((int)commands.shutdown);
            writeToCommand((int)commands.switchOn);
            writeToCommand((int)commands.enableOperation);
        }
        public void stopMotor()
        {
            writeToCommand((int)commands.shutdown);
            disconnect();
        }
        public void rampUp()
        {
            writeToAddress("speedSetPoint", 1);
            writeToAddress("acceleration", 1);
            writeToCommand((int)commands.shutdown);
            writeToCommand((int)commands.switchOn);
            writeToCommand((int)commands.enableOperation);
        }
        public void rampDown()
        {
            writeToAddress("deceleration", 1);
            writeToCommand((int)commands.haltStop);

            while (true)
            {
                if (readFromAddress("outputFrequency") == 0)
                {
                    writeToCommand((int)commands.resetFaults);
                    writeToCommand((int)commands.shutdown);
                    disconnect();
                    break;
                }
            }
        }

        public void resetFaults()
        {
            writeToCommand((int)commands.resetFaults);
        }

        public void disconnect()
        {
            modbusClient.Disconnect();
        }
    }

    public static class VFDFunctions
    {
        public static void StartMotor(EasyModbus.ModbusClient client)
        {
            var start = new vfd(client);
            start.startMotor();
        }

        public static void StopMotor(EasyModbus.ModbusClient client)
        {
            var stop = new vfd(client);
            stop.stopMotor();
        }

        public static void RampUp(EasyModbus.ModbusClient client)
        {
            var rampUp = new vfd(client);
            rampUp.rampUp();
        }

        public static void RampDown(EasyModbus.ModbusClient client)
        {
            var rampDown = new vfd(client);
            rampDown.rampDown();
        }

        public static void InitializeMotor(EasyModbus.ModbusClient client)
        {
            var init = new vfd(client);
            init.initializeMotorAttributes();
        }

        public static void ResetFaults(EasyModbus.ModbusClient client)
        {
            var resetFaults = new vfd(client);
            resetFaults.resetFaults();
        }
    }
}