﻿using System.ServiceProcess;
using System.Text;
using System.Net.Sockets;
using System.Threading;

namespace vfd_service
{

    public partial class VFD_Service : ServiceBase
    {
        private EasyModbus.ModbusClient client = new EasyModbus.ModbusClient("192.168.121.100", 502);

        //Defining service commands
        private enum vfdCommands { startMotor = 128, stopMotor = 129, rampUp = 130, rampDown = 131, initialize = 132, resetFaults = 133 };

        public VFD_Service()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            System.IO.Directory.SetCurrentDirectory(System.AppDomain.CurrentDomain.BaseDirectory);
            //Thread to send telemetry to main application
            Thread SocketThread = new Thread(ClientSend.Client);
            SocketThread.Start();
        }

        protected override void OnCustomCommand(int command)
        {
            base.OnCustomCommand(command);

            switch (command)
            {
                case (int)vfdCommands.startMotor:
                    VFDFunctions.StartMotor(client);
                    break;
                case (int)vfdCommands.stopMotor:
                    VFDFunctions.StopMotor(client);
                    break;
                case (int)vfdCommands.rampUp:
                    VFDFunctions.RampUp(client);
                    break;
                case (int)vfdCommands.rampDown:
                    VFDFunctions.RampDown(client);
                    break;
                case (int)vfdCommands.initialize:
                    VFDFunctions.InitializeMotor(client);
                    break;
                case (int)vfdCommands.resetFaults:
                    VFDFunctions.ResetFaults(client);
                    break;
            }
        }
    }

    //thread to send telemetry back to main application via TCP
    public static class ClientSend
    {
        const string SERVER_IP = "127.0.0.1";
        const int PORT_NUM = 8010;

        public static void Client()
        {
            TcpClient client = new TcpClient(SERVER_IP, PORT_NUM);
            NetworkStream nwStream = client.GetStream();

            while (client.Connected)
            {
                EasyModbus.ModbusClient myClient = new EasyModbus.ModbusClient("192.168.121.100", 502);
                var dataFromVFD = new vfd(myClient);

                string data = dataFromVFD.readFromAddress("outputVelocity").ToString() + "," + dataFromVFD.readFromAddress("driveThermalState").ToString() + "," + dataFromVFD.readFromAddress("outputFrequency") + "," +
                dataFromVFD.readFromAddress("motorThermalState") + "," + dataFromVFD.readFromAddress("motorCurrent") + "," + dataFromVFD.readFromAddress("motorVoltage") + "," +
                dataFromVFD.readFromAddress("totalDriveOperatingTime") + "," + dataFromVFD.readFromAddress("faultCodes") + "," + dataFromVFD.readFromAddress("motorTorque") + "," + dataFromVFD.readFromAddress("driveState");

                dataFromVFD.disconnect();
                byte[] bytesToSend = ASCIIEncoding.ASCII.GetBytes(data);

                nwStream.Write(bytesToSend, 0, bytesToSend.Length);
            }
        }
    }
}