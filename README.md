# README #

This repository contains two VS 2017 solutions: a WPF application (vfd_wpf) and a Windows Service application (vfd_service).  

### What is this repository for? ###

These projects enable communication to the ATV32 Schneider Electric variable frequency drive on the same network through Modbus/TCP.  
* Version 1.0

### How do I get set up? ###

* Summary of setup: 
The vfd_service windows service needs to be installed on the local machine.  Build the solution in Visual Studio, and then
launch the Developer Command Prompt for VS2017.  Navigate to where the build is located, in my case it is:
C:\Users\dgklett\documents\variablefrequencydrive\vfd_service\vfd_service\bin\Debug
Run the following command while in this directory: InstallUtil.exe "vfd_service.exe"
When making changes to the service and rebuilding it, run: InstallUtil.exe /u "vfd_service.exe"
and then run: InstallUtil.exe "vfd_service.exe"
This reinstalls the service to the local machine.  
* Configuration: 
The xmlAccess.cs file in both projects need to changed to reflect the location of the four xml files:
		static XDocument vfdselection = XDocument.Load(@"C:\Users\dgklett\Documents\variablefrequencydrive\vfdselection.xml");
        static XDocument motorselection = XDocument.Load(@"C:\Users\dgklett\Documents\variablefrequencydrive\motorselection.xml");
        static XDocument serviceVFD = XDocument.Load(@"C:\Users\dgklett\Documents\variablefrequencydrive\serviceVFD.xml");
        static XDocument serviceMotor = XDocument.Load(@"C:\Users\dgklett\Documents\variablefrequencydrive\serviceMotor.xml");
Edit the path to where you are storing these files.
* Dependencies: 
EasyModbusTCP NuGet package is required for the Modbus/TCP communication.  