﻿using System.Windows;
using System.Windows.Controls;

namespace vfd_wpf
{
    /// <summary>
    /// Interaction logic for Window4.xaml
    /// </summary>
    public partial class Window4 : Window
    {
        public Window4()
        {
            InitializeComponent();
        }

        private void set_Click(object sender, RoutedEventArgs e)
        {
            xmlAccess.addMotor(motorName.Text.Replace(" ", string.Empty));
            xmlAccess.addMotorSettings(motorName.Text.Replace(" ", string.Empty));
            xmlAccess.addMotorElementValues(motorName.Text.Replace(" ", string.Empty), maxRPM.Text, maxFrequency.Text, horsepower.Text, MaxAmps.Text,
                                        RatedVoltage.Text, Acceleration.Text, Deceleration.Text, SpeedSetPoint.Text);
            ComboBoxItem motor = new ComboBoxItem();
            motor.Content = motorName.Text;
            MainWindow win = (MainWindow)Application.Current.MainWindow;
            win.ComboBox2.Items.Add(motor);
        }
    }
}
