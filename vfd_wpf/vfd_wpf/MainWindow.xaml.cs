﻿using System;
using System.Text;
using System.Windows;
using System.ServiceProcess;
using System.Threading;
using System.Net;
using System.Net.Sockets;
using System.Collections.Generic;
using System.Windows.Controls;
using System.Windows.Data;

namespace vfd_wpf
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        //Defining service commands
        private enum vfdCommands { startMotor = 128, stopMotor = 129, rampUp = 130, rampDown = 131, initialize = 132, resetFaults = 133 };

        public MainWindow()
        {
            InitializeComponent();
            
            Thread tcpThread = new Thread(Server);
            tcpThread.IsBackground = true;
            tcpThread.Start();

            xmlAccess.loadVFDProfiles();
            xmlAccess.loadMotorProfiles();
            
        }

        //Start button:
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            runServiceCommand((int)vfdCommands.startMotor);
        }
        //Stop button:
        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            runServiceCommand((int)vfdCommands.stopMotor); //129 is custom command for stop
        }

        public void runServiceCommand(int command)
        {
            serviceConnect(command, "vfd_service");
        }

        public void serviceConnect(int command, string serviceName)
        {
            ServiceController[] scServices;
            scServices = ServiceController.GetServices();
            foreach (ServiceController scTemp in scServices)
            {
                if (scTemp.ServiceName == serviceName)
                {
                    ServiceController sc = new ServiceController(serviceName);
                     
                    if (sc.Status == ServiceControllerStatus.Stopped)
                    {
                        sc.Start();
                        while (sc.Status == ServiceControllerStatus.Stopped)
                        {
                            Thread.Sleep(1000);
                            sc.Refresh();
                        }
                    }
               
                    if (sc.Status == ServiceControllerStatus.Running)
                    {
                        sc.ExecuteCommand(command); //integer command number for service
                    }
                }
            }
        }

        //Ramp Up:
        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            runServiceCommand((int)vfdCommands.rampUp);
        }
        //Ramp down:
        private void Button_Click_3(object sender, RoutedEventArgs e)
        {
            runServiceCommand((int)vfdCommands.rampDown);
        }
        //Reset faults:
        private void faultReset_Click(object sender, RoutedEventArgs e)
        {
            runServiceCommand((int)vfdCommands.resetFaults);
        }
        //Initialize button:
        private void initialize_Click(object sender, RoutedEventArgs e)
        {
            xmlAccess.setServiceMotor(ComboBox2.SelectedValue.ToString().Substring(ComboBox2.SelectedValue.ToString().LastIndexOf(':') + 2).Replace(" ", string.Empty));
            runServiceCommand((int)vfdCommands.initialize);
        }
        //Thread to retrieve telemetry from service program
        public void Server() 
        {
            //--listen at defined IP and port number---
            const int PORT_NUM = 8010;
            IPAddress localAddr = IPAddress.Parse("127.0.0.1");
            TcpListener listener = new TcpListener(localAddr, PORT_NUM);

            MyLabel: 
            listener.Start();
          
            //---incoming client connected---
            TcpClient client = listener.AcceptTcpClient();

            //---get the incoming data through a network stream---
            NetworkStream nwStream = client.GetStream();
            
            while (client.Connected) 
            {

                byte[] buffer = new byte[client.ReceiveBufferSize];

                if (nwStream.CanRead)
                {
                    try
                    {
                        //---read incoming stream---
                        int bytesRead = nwStream.Read(buffer, 0, client.ReceiveBufferSize);

                        //---convert the data received into a string---
                        string dataReceived = Encoding.ASCII.GetString(buffer, 0, bytesRead);
                        string[] dataArray = dataReceived.Split(',');
                        string speedString = dataArray[0];
                        string driveThermalString = dataArray[1];
                        string outputFreq = dataArray[2];
                        string motTherm = dataArray[3];
                        string motCurrent = dataArray[4];
                        string motVolt = dataArray[5];
                        string operatingT = dataArray[6];
                        string fault = dataArray[7];
                        string torque = dataArray[8];
                        string driveStatus = dataArray[9];

                        //Update labels with current data:
                        Label2?.Dispatcher?.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, new Action(delegate () { Label2.Content = driveThermalString; }));
                        Label1?.Dispatcher?.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, new Action(delegate () { Label1.Content = speedString; }));
                        Label3?.Dispatcher?.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, new Action(delegate () { Label3.Content = outputFreq; }));
                        motorThermalState?.Dispatcher?.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, new Action(delegate () { motorThermalState.Content = motTherm; }));
                        motorCurrent?.Dispatcher?.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, new Action(delegate () { motorCurrent.Content = motCurrent; }));
                        motorVoltage?.Dispatcher?.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, new Action(delegate () { motorVoltage.Content = motVolt; }));
                        operatingTime?.Dispatcher?.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, new Action(delegate () { operatingTime.Content = operatingT; }));
                        faultCode?.Dispatcher?.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, new Action(delegate () { faultCode.Content = fault; }));
                        torqueMotor?.Dispatcher?.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, new Action(delegate () { torqueMotor.Content = torque; }));
                        convertedFaultCode?.Dispatcher?.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, new Action(delegate () { convertedFaultCode.Content = convertFaultCode(fault); }));
                        driveState?.Dispatcher?.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, new Action(delegate () { driveState.Content = driveStatus; }));
                        convertedDriveState?.Dispatcher?.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, new Action(delegate () { convertedDriveState.Content = convertDriveState(driveStatus); }));
                    }
                    catch (System.IO.IOException)
                    {
                        break;
                    }
                }
                else
                {
                    break;
                }
            }
            goto MyLabel;
        }
        
        public string convertFaultCode(string fault)
        {
            switch(fault)
            {
                case "0":
                    return "[No fault] (nOF)";
                case "2":
                    return "[Control Eeprom] (EEF1)";
                case "3":
                    return "[Incorrect config.] (CFF)";
                case "4":
                    return "[Invalid config.] (CFI)";
                case "5":
                    return "[Modbus com.] (SLF1)";
                case "6":
                    return "[int. com.link] (ILF)";
                case "7":
                    return "[Com. network] (CnF)";
                case "8":
                    return "[External flt-LI/Bit] (EPF1)";
                case "9":
                    return "[Overcurrent] (OCF)";
                case "10":
                    return "[Precharge] (CrF)";
                case "11":
                    return "[Speed fdback loss] (SPF)";
                case "12":
                    return "[Load slipping] (AnF)";
                case "16":
                    return "[Drive overheat] (OHF)";
                case "17":
                    return "[Motor overload] (OLF)";
                case "18":
                    return "[Overbraking] (ObF)";
                case "19":
                    return "[Mains overvoltage] (OSF)";
                case "20":
                    return "[1 output phase loss] (OPF1)";
                case "21":
                    return "[Input phase loss] (PHF)";
                case "22":
                    return "[Undervoltage] (USF)";
                case "23":
                    return "[Motor short circuit] (SCF1)";
                case "24":
                    return "[Overspeed] (SOF)";
                case "25":
                    return "[Auto-tuning] (tnF)";
                case "26":
                    return "[Rating error] (InF1)";
                case "27":
                    return "[PWR Calib.] (InF2)";
                case "28":
                    return "[Int.serial link] (InF3)";
                case "29":
                    return "[Int.Mfg area] (InF4)";
                case "30":
                    return "[Power Eeprom] (EEF2)";
                case "32":
                    return "[Ground short circuit] (SCF3)";
                case "33":
                    return "[3out ph loss] (OPF2)";
                case "34":
                    return "[CAN com.] (COF)";
                case "35":
                    return "[Brake control] (bLF)";
                case "38":
                    return "[External fault com.] (EPF2)";
                case "41":
                    return "[Brake feedback] (brF)";
                case "42":
                    return "[PC com.] (SLF2)";
                case "44":
                    return "[Torque/current lim] (SSF)";
                case "45":
                    return "[HMI com.] (SLF3)";
                case "49":
                    return "[LI6=PTC probe] (PtFL)";
                case "50":
                    return "[PTC fault] (OtFL)";
                case "51":
                    return "[Internal- I measure] (InF9)";
                case "52":
                    return "[Internal-mains circuit] (InFA)";
                case "53":
                    return "[Internal- th. sensor] (InFb)";
                case "54":
                    return "[IGBT overheat] (tJF)";
                case "55":
                    return "[IGBT short circuit] (SCF4)";
                case "56":
                    return "[Motor short circuit] (SCF5)";
                case "58":
                    return "[Out. contact. stuck] (FCF1)";
                case "59":
                    return "[Out. contact. open.] (FCF2)";
                case "64":
                    return "[input contactor] (LCF)";
                case "67":
                    return "[IGBT desaturation] (HdF)";
                case "68":
                    return "[Internal-option] (InF6)";
                case "69":
                    return "[internal- CPU] (InFE)";
                case "71":
                    return "[AI3 4-20mA loss] (LFF3)";
                case "73":
                    return "[Cards pairing] (HCF)";
                case "76":
                    return "[Load fault] (dLF)";
                case "77":
                    return "[Bad conf] (CFI2)";
                case "99":
                    return "[Ch.sw. fault] (CSF)";
                case "100":
                    return "[Pr.Underload.Flt] (ULF)";
                case "101":
                    return "[Proc.Overload Flt] (OLC)";
                case "105":
                    return "[Angle error] (ASF)";
                case "107":
                    return "[Safety fault] (SAFF)";
                case "108":
                    return "[FB fault] (FbE)";
                case "109":
                    return "[FB stop flt.] (FbES)";
                default: return "null";
            }
        }

        public string convertDriveState(string fault)
        {
            switch(fault)
            {
                case "0":
                    return "[Auto - tuning](tUn)";
                case "1":
                    return "[In DC inject.] (dCb)";
                case "2":
                    return "[Ready] (rdY)";
                case "3":
                    return "[Freewheel] (nSt)";
                case "4":
                    return "[Drv running] (rUn)";
                case "5":
                    return "[In accel.] (ACC)";
                case "6":
                    return "[In decel.] (dEC)";
                case "7":
                    return "[Current lim.] (CLI)";
                case "8":
                    return "[Fast stop] (FSt)";
                case "9":
                    return "[Mot. fluxing] (FLU)";
                case "11":
                    return "[no mains V.] (nLP)";
                case "12":
                    return "[Active PWR] (PrA)";
                case "13":
                    return "[control.stop] (CtL)";
                case "14":
                    return "[Dec. adapt.] (Obr)";
                case "15":
                    return "[Output cut] (SOC)";
                case "17":
                    return "[UnderV. al.] (USA)";
                case "18":
                    return "[In mfg. test] (tC)";
                case "19":
                    return "[in autotest] (St)";
                case "20":
                    return "[autotest err] (FA)";
                case "21":
                    return "[Autotest OK] (YES)";
                case "22":
                    return "[eeprom test] (EP)";
                case "23":
                    return "[In fault] (FLt)";
                case "25":
                    return "[DCP] (dCP)";
                case "28":
                    return "[SS1 active] (SS1)";
                case "29":
                    return "[SLS active] (SLS)";
                case "30":
                    return "[STO active] (StO)";
                case "31":
                    return "(SMS)";
                case "32":
                    return "(GdL)";
                default:
                    return "NULL";
            }
        }

        private void Button_Click_5(object sender, RoutedEventArgs e)
        {
            Window win3 = new Window3();
            win3.Show();
        }
       
        private void Button_Click_6(object sender, RoutedEventArgs e)
        {
            xmlAccess.setServiceVFD(ComboBox1.SelectedValue.ToString().Substring(ComboBox1.SelectedValue.ToString().LastIndexOf(':') + 2).Replace(" ", string.Empty));
        }

        private void Button_Click_7(object sender, RoutedEventArgs e)
        {
            Window win4 = new Window4();
            win4.Show();
        }

        private void Button_Click_4(object sender, RoutedEventArgs e)
        {
            xmlAccess.removeVFD(ComboBox1.SelectedValue.ToString().Substring(ComboBox1.SelectedValue.ToString().LastIndexOf(':') + 2));
            ComboBox1.Items.Remove(ComboBox1.SelectedItem);
        }

        private void Button_Click_8(object sender, RoutedEventArgs e)
        {
            xmlAccess.removeMotor(ComboBox2.SelectedValue.ToString().Substring(ComboBox2.SelectedValue.ToString().LastIndexOf(':') + 2));
            ComboBox2.Items.Remove(ComboBox2.SelectedItem);
        }
    }

    public struct Data
    {
        public string Name { get; set; }
        public string Value { get; set; }
        public string Units { get; set; }
    }
}
