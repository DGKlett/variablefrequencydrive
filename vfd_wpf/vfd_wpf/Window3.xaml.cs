﻿using System.Windows;
using System.Windows.Controls;

namespace vfd_wpf
{
    /// <summary>
    /// Interaction logic for Window3.xaml
    /// </summary>
    public partial class Window3 : Window
    {
        public Window3()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            xmlAccess.addVFD(vfdname.Text.Replace(" ", string.Empty));
            xmlAccess.addVFDSettings(vfdname.Text.Replace(" ", string.Empty));
            xmlAccess.addVFDAddresses(vfdname.Text.Replace(" ", string.Empty), command.Text, maxRPM.Text, maxFrequency.Text, horsepower.Text,
                                    maxAmps.Text, voltage.Text, acceleration.Text, deceleration.Text, speedSetPoint.Text,
                                    outputFrequency.Text, outputVelocity.Text, driveThermalState.Text, motorThermalState.Text,
                                    totalDriveOperatingTime.Text, totalMotorOperatingTime.Text, motorTorque.Text, motorCurrent.Text,
                                    motorVoltage.Text, faultCodes.Text, driveState.Text);
            ComboBoxItem vfd = new ComboBoxItem();
            vfd.Content = vfdname.Text;
            MainWindow win = (MainWindow)Application.Current.MainWindow;
            win.ComboBox1.Items.Add(vfd);
        }
    }
}
